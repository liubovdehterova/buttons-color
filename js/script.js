/*
Чому для роботи з input не рекомендується використовувати клавіатуру?
Для роботи з input не рекомендується використовувати клавіатуру через обмеженість для автоматизації,
невдало для використання деякими користувачами (наприклад, з обмеженими можливостями),
мовні бар'єри.
*/

const btn = document.querySelectorAll('.btn');

document.addEventListener('keyup', (e) => {
    btn.forEach(elem => {
        elem.style.background = "black";
        if(elem.innerHTML.toLowerCase() === e.key.toLowerCase()) {
            elem.style.background = "blue";
        }
    })
});

